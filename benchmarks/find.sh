for file in $(find ./* -type f); do
	xzcat $file | grep -q \"$1\"
	if [ $? -eq 0 ]; then
		echo $file
	fi
done
